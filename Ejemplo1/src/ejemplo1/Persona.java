package ejemplo1;

/**
 *
 * @author jose
 */
public class Persona {
	String nombre;
	String rut;

	public Persona() {
		nombre = "";
		rut = "";
	}

	public Persona(String nombre, String rut) {
		this.nombre = nombre;
		this.rut = rut;
	}

	public Persona(String nombre) {
		this.nombre = nombre;
		rut = "";
	}
	
	public void caminar(int x){
		System.out.println("He caminado +"+x+" metros");
	}
	
	
}
