package ejemplo1;

/**
 * Este es un ejemplo básico que permite visualizar una clase, el objeto y el 
 * uso del constructor. Se han omitido algunos elementos para el mejor
 * entendimiento de los elementos anteriores.
 * @author jose
 */
public class Ejemplo1 {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		int a = 5;
		Persona persona1 = new Persona("Juan", "11-1");
		
		persona1.caminar(a);
		
		System.out.println(persona1.nombre+" "+persona1.rut);
	}
	
}
