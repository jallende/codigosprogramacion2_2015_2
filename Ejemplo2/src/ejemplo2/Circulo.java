package ejemplo2;

/**
 *
 * @author jose
 */
public class Circulo {
	private int radio;

	public Circulo(int radio) {
		this.radio = radio;
	}

	public Circulo() {
	}
	
	
	
	public void setRadio(int r){
		if (r>=0) {
			radio = r;
		}
		else System.out.println("El radio no puede ser menor a 0");
	}
	
	public int getRadio(){
		return radio;
	}
	
	public double calcularArea(){
		return 3.14*radio*radio;
	}
	
	public double calcularPerimetro(){
		return 2* 3.14*radio;
	}
	
	public void mostrarCirculo(){
		System.out.println("Radio:"+radio);
		System.out.println("Area:"+calcularArea());
		System.out.println("Perimetro"+this.calcularPerimetro());
	}
	
}
