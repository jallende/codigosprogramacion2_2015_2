package ejemplo2;

import paquete2.Cuadrado;

/**
 *
 * @author jose
 */
public class Ejemplo2 {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		Circulo c = new Circulo();
		int x = c.getRadio();
		c.setRadio(66);
		System.out.println("radio= "+c.getRadio());
		System.out.println("x:"+x);
		
		c.mostrarCirculo();
		
		double area = c.calcularArea();
		System.out.println("area:"+area);
		
		Cuadrado l = new Cuadrado();
		
		l.setLado(4);
		
		Punto p1 = new Punto(3, 4);
		Punto p2 = new Punto(2, 8);
		
		System.out.println(p1.compararPuntos(p2));
		
		
	}
	
}
