/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import ejemplo3.*;
import paqueteClaseAbstracta.ClaseAbstracta;

/**
 *
 * @author jose
 */
public class Ejemplo3 {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		Auto a = new Auto(40, 5, 0, "verde");
		
		a.avanzar(10);
		a.cambiarColor("naranjo");
		
		//a.avanzar();
		System.out.println(a.funcion(4, 4));
		System.out.println(a.funcion(3.6, 4.6));
		System.out.println(a.funcion(2, 10.0));
		a.funcion(5);
		
		ClaseAbstracta obj = new ClaseAbstracta();
		
	}
	
}
