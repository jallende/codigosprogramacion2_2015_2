package ejemplo3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author jose
 */
public class Auto extends VehiculoTerrestre{
	private int capMaletero;
	private int capPasajeros;

	public Auto(int capMaletero, int capPasajeros, int velocidad, String color) {
		super(velocidad, color);
		this.capMaletero = capMaletero;
		this.capPasajeros = capPasajeros;
	}

	public Auto(int capMaletero, int capPasajeros) {
		super();
		this.capMaletero = capMaletero;
		this.capPasajeros = capPasajeros;
	}
	
	public void avanzar(int x){
		System.out.println("estamos avanzando...");
		setVelocidad(x);
		System.out.println(this.getVelocidad());
	}
	
	public void avanzar(){
		System.out.println("avanzo continuamente");
	}
	
	public void cambiarColor(String c){
		color = c;
		System.out.println("nuevo color: "+color);
	}
	
	public void funcion(int x){
		System.out.println("x="+x);
	}
	
	public int funcion(int x, int y){
		return x+y;
	}
	
	public double funcion(double x, double y){
		return x/y;
	}
	
	public double funcion(double x, int y){
		return x*y;
	}
	
	public double funcion(int x, double y){
		return x*y;
	}
	
	
	
	
	
}
