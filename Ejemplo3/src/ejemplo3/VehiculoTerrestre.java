package ejemplo3;

/**
 *
 * @author jose
 */
public class VehiculoTerrestre {
	private  int velocidad;
	protected String color;

	public VehiculoTerrestre(int velocidad, String color) {
		this.velocidad = velocidad;
		this.color = color;
	}

	public VehiculoTerrestre() {
	}

	
	
	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
	
}
